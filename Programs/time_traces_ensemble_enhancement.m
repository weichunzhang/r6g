clc
clear
close all

% In the program, we plot the time traces to demonstrate ensemble signal
% enhancement for one nanorod.

%% plot settings
LineWidth = 1.5;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 1;
BinTime = 0.1; % In the unit of s.

%% Read data
folder = '..\data\single_photon_data\';

MeOH = load(strcat(folder,'MeOH_NR0_1.5uW.t3r.dat'));
R6G = load(strcat(folder,'R6G_NR0_1.5uW_again2.t3r.dat'));
NR = load(strcat(folder,'MeOH_NR2_1.5uW.t3r.dat'));
NR_R6G = load(strcat(folder,'R6G_NR2_1.5uW.t3r.dat'));

%% plot
trace_length_MeOH = ceil(max(MeOH)); % Length of the time trace in the unit of seconds.
trace_MeOH = histcounts(MeOH,'Binwidth',BinTime,'BinLimits',[0,trace_length_MeOH])/BinTime;
% Number of counts in bin time divided by the bin time: counts/s.

trace_length_R6G = ceil(max(R6G)); % Length of the time trace in the unit of seconds.
trace_R6G = histcounts(R6G,'Binwidth',BinTime,'BinLimits',[0,trace_length_R6G])/BinTime;
% Number of counts in bin time divided by the bin time: counts/s.

trace_length_NR = ceil(max(NR)); % Length of the time trace in the unit of seconds.
trace_NR = histcounts(NR,'Binwidth',BinTime,'BinLimits',[0,trace_length_NR])/BinTime;
% Number of counts in bin time divided by the bin time: counts/s.

trace_length_NR_R6G = ceil(max(NR_R6G)); % Length of the time trace in the unit of seconds.
trace_NR_R6G = histcounts(NR_R6G,'Binwidth',BinTime,'BinLimits',[0,trace_length_NR_R6G])/BinTime;
% Number of counts in bin time divided by the bin time: counts/s.

% plot(BinTime:BinTime:trace_length_MeOH,trace_MeOH,BinTime:BinTime:trace_length_R6G,trace_R6G,...
%    BinTime:BinTime:trace_length_NR,trace_NR,BinTime:BinTime:trace_length_NR_R6G,trace_NR_R6G,...
%    'LineWidth',LineWidth);

plot(BinTime:BinTime:trace_length_R6G,trace_R6G,...
    BinTime:BinTime:trace_length_NR,trace_NR,BinTime:BinTime:trace_length_NR_R6G,trace_NR_R6G,...
    'LineWidth',LineWidth); % Do not plot the dark count.

xlabel('Time (s)');
ylabel('Fluorescence (counts/s)');
xlim([0 20]);

% text(8,500,'MeOH','FontSize',FontSize,'FontName',FontName);
text(8,1400,'R6G','FontSize',FontSize,'FontName',FontName);
text(8,2600,'Nanorod','FontSize',FontSize,'FontName',FontName);
text(8,6000,'R6G + nanorod','FontSize',FontSize,'FontName',FontName);

set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

% grid
if ifsave == 1
    saveas(gcf, '..\images\time_traces_ensemble_enhancement', 'svg')
end






