clc
close all
clear

folder = '..\data\results\';
ifsave = 1; % save the image or not?
LineWidth= 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';

A = xlsread(strcat(folder,'SPR_versus_enhancement_factor_no_NR13_interphoton_power_dependence.xlsx'),'NR1','B1:D3');

power = A(1,:);
fac = 1000*(A(2,:)-1);
err_fac = A(3,:)*1000;
errorbar(power,fac,err_fac,'ob',...
    'LineWidth',LineWidth,'MarkerSize',8);
xlim([0 2]);
ylim([0 2000]);
xlabel('Laser power (uW)');
ylabel('Enhancement factor');

set(gca,'Fontname','SansSerif');
set(gca,'FontSize',FontSize);
set(gca,'linewidth',Border);
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

% grid
if ifsave == 1
    saveas(gcf, '..\images\enhancement_factor_vs_power_NR1', 'svg');
end