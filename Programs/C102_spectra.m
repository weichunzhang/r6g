% test spectra load

close all
clear
clc

addpath('Z:\Weichun\Matlab_programs\Spectra\');
folder = '..\data\spectra\';


LineWidth= 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 0;

%% Spectra of R6G.
% absorption
data = dlmread(strcat(folder,'C102_MeOH_63uM.csv'),',',2,0,[2 0 302 1]);
wa=data(:,1);
a=data(:,2);
% emission
data = dlmread(strcat(folder,'C102_MeOH_63uM_360nm_excitation_low_PMT.csv'),',',2,0,[2 0 302 1]);
we=data(:,1);
e=data(:,2);
clear data

%%
figure(1)

plot(wa,a./max(a),'b',we,e./max(e),'r--','LineWidth',LineWidth);
hold all
% plot(wa,a./max(a),'color',[0 0.5 1],'LineWidth',LineWidth);
% plot([775 775],[-0.1 1.1],':k','LineWidth',LineWidth) % laser line
ylim([0 1.05])
xlim([200 800])
% Draw a vertical arrow
pos = get(gca, 'Position'); % Get the position of the current axes.
X = [(775/2-200)/600*pos(3)+pos(1) (775/2-200)/600*pos(3)+pos(1)];
Y = [(0.9-0)/1.05*pos(4)+pos(2) (0.75-0)/1.05*pos(4)+pos(2)];
annotation('arrow',X,Y,'color','b','LineWidth',LineWidth);

% labels and axis
% set(gcf,'units','centimeters')
% set(gcf,'position',[8 8 16 9])
% % set(gcf,'position',[482   214   956   815])

% l=legend('Ensemble GNR','Qdot 1P absorption','Qdot emission');
% legend('boxoff');
% set(l,'location','northwest')
set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
xlabel('Wavelength (nm)','FontSize',FontSize)
ylabel('Normalized intenstiy ','FontSize',FontSize)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf, '..\images\C102_spectra_matlab', 'svg');
end
