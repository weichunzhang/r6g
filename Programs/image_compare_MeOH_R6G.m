clear
close all
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 1;
FontName = 'SansSerif';

%% Plot image
h = figure;
x = 40:0.1:60;
y = 20:0.1:40;
A = dlmread('../data/images/NR771_MeOH_776nn_1.5uW_circular_4_612SP.dat',',',4,0);
B = dlmread('../data/images/NR771_R6G_776nn_1.5uW_circular_4_612SP.dat',',',4,0);
ax1 = subplot(1,2,1);
imagesc(x,y,A);
% colorbar
colormap 'jet'
lim1 = caxis;
xticks([]);
yticks([]);
axis image
title('Nanorods in MeOH');

hold all
% Add a scale bar
scalebar_x = [42 44];
scalebar_y = [39 39];
plot(scalebar_x,scalebar_y,'Linewidth',LineWidth,'Color','white');
t = text(scalebar_x(1)-0.8,scalebar_y(1) - 1.5,'2 \mum');
set(t,'Linewidth',LineWidth,'Color','white');

ax2 = subplot(1,2,2);
imagesc(x,y,B);
lim2 = caxis;
c = colorbar;
c.Label.String = 'counts/ms';

caxis(ax1,[lim1(1) lim2(2)]);
caxis(ax2,[lim1(1) lim2(2)]);
xticks([]);
yticks([]);
axis image
title('Nanorods in R6G');

%% Output settings
set(h,'units','centimeters')
set(h,'position',[10 5 18.2 8.5])
set(ax1,'units','centimeters');
set(ax2,'units','centimeters');

set(ax1,'position',[0.5 0.5 8 7]);
set(ax2,'position',[8 0.5 8 7]);
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName)

%% Image export
if ifsave == 1
    saveas(gcf, '..\images\image_compare_MeOH_R6G', 'svg');
end
