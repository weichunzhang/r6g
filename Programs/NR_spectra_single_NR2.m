% test spectra load

close all
clear
clc

addpath('Z:\Weichun\Matlab_programs\Spectra\');
folder = '..\data\spectra\';

ifsave = 1; % save the image or not?
LineWidth= 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';

%% Single NR spectrum

low = 630;
high = 832;
BG1 = load(strcat(folder,'NR771_MeOH_532nm_20uW_circular_NR_0.txt'),'txt');
ind1 = find(BG1(:,1)>low&BG1(:,1)<high);
wl = BG1(ind1,1);
BG = BG1(ind1,2);

NR_single = load(strcat(folder,'NR771_MeOH_532nm_20uW_circular_NR_2.txt'),'txt');
NR_single_net = NR_single(ind1,2)-BG;

% Response function
corr = load(strcat(folder,'spectral_response_20171027.txt'),'txt');
ind2 = find(corr(:,1)>low&corr(:,1)<high);
response = corr(ind2,2);

NR_single_corr = NR_single_net./response; % The measured spectra and the spectrum for correction have the same range.
plot(wl,NR_single_corr/max(NR_single_corr),'c','LineWidth',LineWidth);
hold all
%% Lorentzian fit
energy_in = 1./wl;
beta0 = [1 1/780 1/740-1/800]; % Initial value for fitting % FWHM = 2*beta(3).
[beta,R,J,CovB] = nlinfit(energy_in,NR_single_corr/max(NR_single_corr),'lorentz',beta0);
spec_fit = lorentz(beta,energy_in);
SPR = 1/beta(2);
FWHM = 1/(beta(2)+beta(3))-SPR;
beta_ci = nlparci(beta,R,'jacobian',J,'alpha',0.05); % Confidence interval in the unit of 1/lambda
SPR_ci_up = 1/beta_ci(2,1) - SPR;
SPR_ci_down = SPR - 1/beta_ci(2,2);

plot(wl,spec_fit,'m','LineWidth',LineWidth);

SPR_ci_up_str = num2str(SPR_ci_up,2);
SPR_str = num2str(SPR,5);
% str_plus_minus ='$$\pm$$';
% str = strcat('{SPR = }',SPR_str,str_plus_minus,SPR_ci_up_str, '{ nm}');
% text(610, 1/2, str, 'interpreter','latex','FontSize',FontSize,'Fontname','SansSerif');

% plot([775 775],[-0.1 1.1],':k','LineWidth',LineWidth) % laser line

ylim([0 1.05])
xlim([low high])

set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
xlabel('Wavelength (nm)','FontSize',FontSize)
ylabel('Normalized intenstiy ','FontSize',FontSize)
% legend('Nanorod single','Lorentzian fit''Laser');
% legend('location','northwest');
if ifsave == 1
    saveas(gcf, '..\images\NR_spectra_bulk_matlab', 'svg');
end
