clc
clear
close all

% In the program, we calculate the signal enhancement factors for each
% nanorod.

%% Read data
folder = '..\data\single_photon_data\';

MeOH = load(strcat(folder,'MeOH_NR0_1.5uW.t3r.dat'));
R6G = load(strcat(folder,'R6G_NR0_1.5uW_again2.t3r.dat'));
NR = load(strcat(folder,'MeOH_NR2_1.5uW.t3r.dat'));
NR_R6G = load(strcat(folder,'R6G_NR2_1.5uW.t3r.dat'));

%% Calculations
ip_MeOH = diff(MeOH); % interphoton times.
[mu_MeOH, ci_MeOH] = expfit(ip_MeOH);
count_MeOH = 1./mu_MeOH; % convert interphoton time to count rate.
err_MeOH = (ci_MeOH(2)-mu_MeOH)./(mu_MeOH.^2);

ip_R6G = diff(R6G);
[mu_R6G, ci_R6G] = expfit(ip_R6G);
count_R6G = 1./mu_R6G; % convert interphoton time to count rate.
err_R6G = (ci_R6G(2)-mu_R6G)./(mu_R6G.^2);

ip_NR = diff(NR);
[mu_NR, ci_NR] = expfit(ip_NR);
count_NR = 1./mu_NR; % convert interphoton time to count rate.
err_NR = (ci_NR(2)-mu_NR)./(mu_NR.^2);

net_NR = count_NR - count_MeOH;
err_net_NR = sqrt(err_NR^2 + err_MeOH^2);

ip_NR_R6G = diff(NR_R6G);
[mu_NR_R6G, ci_NR_R6G] = expfit(ip_NR_R6G);
count_NR_R6G = 1./mu_NR_R6G; % convert interphoton time to count rate.
err_NR_R6G = (ci_NR_R6G(2)-mu_NR_R6G)./(mu_NR_R6G.^2);

net_R6G = count_R6G - count_MeOH;
err_net_R6G = sqrt(err_MeOH^2+err_R6G^2); % Uncertainty propagation;

net_NR_R6G = count_NR_R6G - count_NR;
err_net_NR_R6G = sqrt(err_NR_R6G^2 + err_NR^2);

net_enh = net_NR_R6G - net_R6G;
err_net_enh = sqrt(err_net_NR_R6G^2 + err_net_R6G^2);

% Enhancement factor and uncertainty
enh_fact = net_NR_R6G/net_R6G
err_enh_fact = enh_fact*sqrt((err_net_NR_R6G/net_NR_R6G)^2+(err_net_R6G/net_R6G)^2) % Uncertainty propagation;

enh_fact1 = net_enh/net_R6G
err_enh_fact1 = enh_fact*sqrt((err_net_enh/net_enh)^2+(err_net_R6G/net_R6G)^2) % Uncertainty propagation;






