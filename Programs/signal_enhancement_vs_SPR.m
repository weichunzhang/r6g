close all
clear
clc

folder = '..\data\results\';
ifsave = 1; % save the image or not?
LineWidth= 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';

A = xlsread(strcat(folder,'SPR_versus_enhancement_factor_no_NR13_interphoton_power_dependence.xlsx'),1,'B2:E31');

errorbar(A(:,1),(A(:,3)-1)*1000,A(:,4)*1000,'ob',...
    'LineWidth',LineWidth,'MarkerSize',8);
xlabel('Plasmon resonance (nm)');
ylabel('Enhancement factor');
xlim([720 810]);
set(gca,'Fontname','SansSerif');
set(gca,'FontSize',FontSize);
set(gca,'linewidth',Border);
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

% grid
if ifsave == 1
    saveas(gcf, '..\images\enhancement_factor_vs_SPR', 'svg');
end
