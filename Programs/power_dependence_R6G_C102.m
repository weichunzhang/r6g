close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 0;
%% Read data
folder = '..\data\power_dependence\';

bg = dlmread(strcat(folder,'Power_dependence_Sample=68_9uM_R6G_MeOH_776nm_circular_lambda=776nm_Meas_at_12hs35m_TimeTrace_Power=0uW.dat'),',',2,0);

bkg_mean = mean(bg)*100;
bkg_std = std(bg)*100;

file = dir(strcat(folder,'Power_dependence_Sample=68_9uM_R6G_MeOH_776nm_circular_lambda=776nm_Meas_at_12hs35m_averaged.dat'));

a = csvread(strcat(folder,file.name),1,0);
pw = a(3:end,1); % power at bfp

c2 = a(3:end,3); % counter 2
e2 = a(3:end,5);

ch2 = (c2 - bkg_mean)*100/68.9; % Convert to 100 uM
e = sqrt((e2*100/68.9).^2+(bkg_std*100/68.9).^2); % propagation of uncertainty.

errorbar(pw,ch2,e,'sb','LineWidth',LineWidth);
hold all
%% fit
log_power = log(pw); % Power for fitting
log_cnt = log(ch2); % counts for fitting.

[p_poly,S] = polyfit(log_power,log_cnt,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict = polyval(p_poly,log_power);
% yresid = log_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(log_cnt)-1) * var(log_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power),exp(cnt_predict),'b--','LineWidth',LineWidth);

hold all

%% pulsed laser.

A = dlmread(strcat(folder,'Power_dependence_Sample=8mM_C102_MeOH_774nm_solution_lambda=774nm_Meas_at_19hs32m_TimeTrace_Power=0uW.dat'),',',1,0);
% B = dlmread(strcat(folder,'Power_dependence_Sample=8mM_C102_MeOH_774nm_solution_lambda=774nm_Meas_at_19hs32m_TimeTrace_Power=32.75uW.dat'),',',1,0);
C = dlmread(strcat(folder,'Power_dependence_Sample=8mM_C102_MeOH_774nm_solution_lambda=774nm_Meas_at_19hs32m_TimeTrace_Power=220.29999999999998uW.dat'),',',1,0);
D = dlmread(strcat(folder,'Power_dependence_Sample=8mM_C102_MeOH_774nm_solution_lambda=774nm_Meas_at_19hs32m_TimeTrace_Power=1289.1999999999998uW.dat'),',',1,0);
E = dlmread(strcat(folder,'Power_dependence_Sample=8mM_C102_MeOH_774nm_solution_lambda=774nm_Meas_at_19hs32m_TimeTrace_Power=2130.0uW.dat'),',',1,0);

dark = mean(A(2,:))*100;
std_dark = std(A(2,:))*100;
% sqrt((std_dark*100/7830).^2+(100*std(B(2,:))*100/7830).^2) 
% count = [100*mean(B(2,:))-dark 100*mean(C(2,:))-dark 100*mean(D(2,:))-dark 100*mean(E(2,:))-dark]*100/7830; % cps, convert to 100 uM
count = [100*mean(C(2,:))-dark 100*mean(D(2,:))-dark 100*mean(E(2,:))-dark]*100/7830; % cps, convert to 100 uM
std_count = [sqrt((std_dark*100/7830).^2+(100*std(C(2,:)*100/7830)).^2) sqrt((std_dark*100/7830).^2+(100*std(D(2,:))*100/7830).^2) sqrt((std_dark*100/7830).^2+(100*std(E(2,:))*100/7830).^2)];
% power = [32.75 220.29999999999998 1289.1999999999998 2130.0]/80;
power = [220.29999999999998 1289.1999999999998 2130.0]/80;
errorbar(power,count,std_count,'or','LineWidth',LineWidth);

%% fit
log_power_1 = log(power); % Power for fitting
log_cnt_1 = log(count); % counts for fitting.

[p_poly_1,S_1] = polyfit(log_power_1,log_cnt_1,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict_1 = polyval(p_poly_1,log_power_1);
% yresid = log_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(log_cnt)-1) * var(log_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power_1),exp(cnt_predict_1),'r--','LineWidth',LineWidth);

xlim([1 100]);
ylim([1 1e5]);

set(gca,'yscale','log')
set(gca,'xscale','log')
legend('Data, R6G',strcat('{Fit. Slope = }',num2str(p_poly(1))),'Data, C102',strcat('{Fit. Slope = }',num2str(p_poly_1(1))));
% legend('location','northwest')
legend('location','southeast')
xlabel('Excitation power(\muW)')
ylabel('Fluorescence (counts/s)')
set(gca,'xtick',[1 10 100]);
set(gca,'ytick',[1e0 1e1 1e2 1e3 1e4 1e5]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)

% grid
if ifsave == 1
    saveas(gcf, '..\images\saturation_curve', 'svg')
end
% %% Prediction at 2 nW
% [Y delta] = polyval(p_poly,log(0.002),S);
% pre_2nW = exp(Y)
% pre_err = pre_2nW*delta
