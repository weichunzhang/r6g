clear
close all
clc
%% Use only xz and yz scan.
%% x
x1 = 237.0;
x1_err = 1.4;
x2 = 259.4;
x2_err = 1.1;

x = (x1+x2)/2;
x_err = sqrt((x1_err)/2^2+(x2_err/2)^2);

%% z
z1 = 731.1;
z1_err = 4.4;
z2 = 736.4;
z2_err = 3.1;

z = (z1+z2)/2;
z_err = sqrt((z1_err/2)^2+(z2_err/2)^2);

Vconf = (pi/2)^1.5*x*x*z
Vconf_err = Vconf*sqrt((x_err/x)^2 + (x_err/x)^2 + (z_err/z)^2)

Veff = pi^1.5*x*x*z
Veff_err = Veff*sqrt((x_err/x)^2 + (x_err/x)^2 + (z_err/z)^2)

c = 5340000; % concentration, nM
NA = 6.02214086e23; % avogadro constant
N = c*1e-9*Vconf*1e-24*NA; % number of molecules in the focal volume.
N_err = c*1e-9*Vconf_err*1e-24*NA; % uncertainty.

% pre_2nW = 19.6650;
% pre_err = 3.0541; % count rate from the solution.
% 
% mo_br = pre_2nW/N % molecular brightness
% mo_br_err = mo_br*sqrt((pre_err/pre_2nW)^2 + (N_err/N)^2)